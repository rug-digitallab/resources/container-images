# Report Generator

An image with the `reportgenerator` (https://github.com/danielpalme/ReportGenerator) tool pre-installed. Also includes a `generate_report.sh` script, located in `/opt/generate_report.sh` to ease the merging of 
jacoco reports into a singular cobertura file.

## Usage

Include the following in your `.gitlab-ci.yml` file:

```yaml
stages:
  - coverage

coverage:
  stage: coverage
  image: registry.gitlab.com/rug-digitallab/resources/container-images/report-generator:latest
  script:
    - /opt/generate_report.sh
  coverage: '/Line Coverage:\s*([0-9\.]+)%/'
  artifacts:
    paths:
      - "Cobertura.xml"
    reports:
      coverage_report:
        coverage_format: cobertura
        path: "Cobertura.xml"
```
