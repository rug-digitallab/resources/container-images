#!/bin/bash

# This scripts uses 'reportgenerator' (https://github.com/danielpalme/ReportGenerator) to merge the jacoco reports
# into a singular cobertura report, which GitLab needs for code coverage visualization. The script automatically
# finds kotlin and java source code folders and their corresponding jacoco reports. 
#
# Usage:
#  $ ./generate.report.sh

# Base directory where projects are located
base_dir=$CI_PROJECT_DIR

# List of jacoco.xml files and corresponding source directories
jacoco_reports=""
source_dirs=""

while read -r -d '' project_source_dir; do
    # Determine the project directory
    project_dir=$(dirname $(dirname $(dirname "$project_source_dir")))

    # Find the Quarkus jacoco.xml file or non-Quarkus jacocoTestReport.xml in the project directory
    jacoco_file=$(find "$project_dir/build" -type f \( -name "jacoco.xml" -o -name "jacocoTestReport.xml" \) -print -quit)

    # Check if the jacoco file exists for the project
    if [ -f "$jacoco_file" ]; then
        # Append to jacoco_reports and source_dirs
        jacoco_reports+="${jacoco_file};"
        source_dirs+="${project_source_dir};"
    fi
done < <(find "$base_dir" -type d -and \( -path "*/src/main/kotlin" -or -path "*/src/main/java" \) -print0) # See: https://github.com/koalaman/shellcheck/wiki/SC2044

# Remove the last semicolon from jacoco_reports and source_dirs
jacoco_reports=${jacoco_reports%;}
source_dirs=${source_dirs%;}

# Run the final reportgenerator command, generate Cobertura.xml and index.html
reportgenerator \
    -reports:"${jacoco_reports}" \
    -targetdir:"${CI_PROJECT_DIR}" \
    -sourcedirs:"${source_dirs}" \
    -reporttypes:"Html;Cobertura"

# Change absolute paths in the cobertura report to relative paths
sed -i "s#${CI_PROJECT_DIR}/##g" $CI_PROJECT_DIR/Cobertura.xml

# Print the line coverage so GitLab can extract the coverage percentage
echo -n 'Line Coverage: ' && cat $CI_PROJECT_DIR/index.html | sed -n '/Line coverage:/ {n; s/.*>\(.*\)<\/td.*/\1/p}'

