# Gradle Node.js
An image with the `node` preinstalled alongside Gradle.

# Usage
Include the following in your `.gitlab-ci.yml` file:

```yaml
image: registry.gitlab.com/rug-digitallab/resources/container-images/gradle-nodejs:latest
```
