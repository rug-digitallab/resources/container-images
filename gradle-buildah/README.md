# Gradle Buildah
An image with `buildah` preinstalled alongside Gradle.

# Usage
Include the following in your `.gitlab-ci.yml` file:

```yaml
image: registry.gitlab.com/rug-digitallab/resources/container-images/gradle-buildah:latest
```

