# Sonar Export

This image contains the [`sarif-tools`](https://github.com/microsoft/sarif-tools) and [`sonar-tools`](https://github.com/okorach/sonar-tools) packages, to facilitate exporting scan results from Sonarcloud to GitLab Code Quality. This is used in the CI pipeline to expose Sonarcloud results in the GitLab UI.

## Usage

Typical usage of this container has two steps:

```bash
sonar-findings-export -k "project_key" -o "organisation" -b "branch" --format=sarif > output.sarif
sarif codeclimate -o gitlab-report.json --trim "file:///" output.sarif
```
