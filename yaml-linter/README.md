# Yaml linter
An image with the Python `yamllint` (https://pypi.org/project/yamllint) package installed. The image can be used to verify valid YAML syntax, and to perform quality checks like missing newlines or redundant whitespaces.

# Usage
Include the following in your `.gitlab-ci.yml` file:

```yaml
stages:
  - lint

yaml-lint:
  stage: lint
  script:
    - yamllint -d relaxed .
```