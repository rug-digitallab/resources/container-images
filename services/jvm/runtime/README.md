# JVM runtime
This service is used as a base layer of the runtime stage of microservices within the Digital Lab toolsuite using the JVM.
Within this image the actual microservices will be running when using the JVM

It is based on Red Hat's UBI image and contains OpenJDK17 without compilation infrastructure. The user within this image is set to 185 and any application files should be owned by user 185.
