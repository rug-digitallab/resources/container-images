# JVM build
This service is used as a base layer of the build stage of microservices within the Digital Lab toolsuite using the JVM.

It is based on Red Hat's UBI image and contains OpenJDK17 with compilation infrastructure. The user within this image is set to root.
